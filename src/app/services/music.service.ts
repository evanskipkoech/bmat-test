import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class MusicService {

  db = '../assets/data/sound_recordings.json';

  constructor(private http: HttpClient) { }

  getSongs() {
    return this.http.get(this.db, { responseType: 'json' });
  }

}
