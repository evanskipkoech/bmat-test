import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar, MatTableDataSource } from '@angular/material';
import * as moment from 'moment';
import { Song } from 'src/app/models/song.model';
import { MusicService } from 'src/app/services/music.service';
@Component({
  selector: 'app-song-list',
  templateUrl: './song-list.component.html',
  styleUrls: ['./song-list.component.scss']
})

export class SongListComponent implements OnInit {

  constructor(private _musicSrv: MusicService, public snackBar: MatSnackBar) { }

  @ViewChild('csvReader') csvReader: any;

  displayedColumns: string[] = ['artist', 'title', 'isrc', 'duration'];
  datasource_songs: MatTableDataSource<any>;

  songs: any = [];
  mappedSongs: any = [];
  records = [];
  dbCatalogue;
  isValid: boolean = false;
  dbRecordsMap = new Map();

  ngOnInit() {
    this.getMusicData();
  }

  getMusicData() {
    this._musicSrv.getSongs().subscribe((data) => {
      this.songs = data;
      this.dbHash(this.songs);
    });
  }

  filterCatalogue(filterValue: string) {
    this.datasource_songs.filter = filterValue.trim().toLowerCase();
  }

  dbHash(songArray) {

    this.mappedSongs = [];

    Object.values(songArray).map((record_result: any) => {
      this.dbRecordsMap.set(record_result.artist + record_result.title + record_result.isrc, record_result);
    })
    // console.log('Records: ', this.dbRecordsMap);
    this.dbRecordsMap.forEach((value, key) => {
      this.mappedSongs.push(value)
    })

    console.log('Mapped: ', this.mappedSongs);

    this.datasource_songs = new MatTableDataSource(this.mappedSongs)
  }

  uploadListener($event: any): void {

    let files = $event.srcElement.files;

    if (this.isValidCSVFile(files[0]) && files !== "") {

      let input = $event.target;
      let reader = new FileReader();
      reader.readAsText(input.files[0]);

      reader.onload = () => {
        let csvData = reader.result;

        let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);
        let headersRow = this.getInputSongHeaders(csvRecordsArray);
        this.records = this.getInputSongData(csvRecordsArray, headersRow.length);

      };

      reader.onerror = function () {
        console.log('error is occured while reading file!');
      };

    } else {
      this.snackBar.open('Please import valid .csv file.', 'CLOSE', { duration: 3500, verticalPosition: 'top' });
      this.fileReset();
    }
  }

  getInputSongHeaders(csvRecordsArr: any) {
    let headers = (csvRecordsArr[0]).split(',');
    let headerArray = [];
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  getInputSongData(csvRecordsArray: any, headerLength: any) {

    let songsArray = [];
    let curruntRecord;
    const regex = /\".*\"/;

    for (let i = 1; i < csvRecordsArray.length; i++) {

      if (csvRecordsArray[i].match(regex)) {
        let str = csvRecordsArray[i].match(regex).input;
        let subst = str.match(regex)[0].replace(/,/g, ' -').replace(/\"/g, '');
        const result = str.replace(regex, subst);

        curruntRecord = result.split(',')

      } else {
        curruntRecord = (csvRecordsArray[i]).split(',');
      }

      if (curruntRecord.length == headerLength) {
        let csvRecord: Song = new Song();
        csvRecord.artist = curruntRecord[0].trim();
        csvRecord.title = curruntRecord[1].trim();
        csvRecord.isrc = curruntRecord[2].trim();
        csvRecord.duration = curruntRecord[3].trim();
        songsArray.push(csvRecord);
      }
    }
    console.log('Song Array: ', songsArray);

    return songsArray;
  }

  fileReset() {
    this.csvReader.nativeElement.value = "";
    this.records = [];
  }

  isValidCSVFile(file: any) {
    if (file.name.endsWith(".csv")) {
      this.isValid = true;
    }
    return this.isValid;
  }

  processCsvFile() {

    console.log('DB Map before: ', this.dbRecordsMap);

    if (this.records.length !== 0) {

      console.log('Processing input CSV');
      console.log('Start processing files...', this.records);

      this.records.forEach(candidate => {
        let artist = candidate.artist;
        let title = candidate.title;
        let isrc = candidate.isrc;

        if (this.dbRecordsMap.has(artist + title + isrc)) {

          let dbrecord = this.dbRecordsMap.get(artist + title + isrc);

          if (dbrecord.isrc == '' && candidate.isrc != '') {
            dbrecord.isrc = candidate.isrc
          }
          if (dbrecord.duration == '' && candidate.duration != '') {
            dbrecord.duration = candidate.duration
          }

          this.dbRecordsMap.set(artist + title + isrc, dbrecord);

        } else if (!this.dbRecordsMap.has(artist + title + isrc)) {
          this.dbRecordsMap.set(artist + title + isrc, candidate);
        }
      })
      // Get an array to pass to make the new hash to display
      let updatedSongs: any = []
      this.dbRecordsMap.forEach((value, key) => {
        updatedSongs.push(value)
      })

      this.fileReset();
      this.dbRecordsMap.clear();
      console.log('DB Map cleared: ', this.dbRecordsMap);

      this.dbHash(updatedSongs);

    } else {
      this.snackBar.open('Error: Please upload a valid CSV', 'CLOSE', { duration: 3500, verticalPosition: 'top' });
    }

  }

  songLength(duration) {
    const song_duration = moment.utc(duration * 1000).format('m:ss');
    if (duration > 0) {
      return song_duration;
    }
  }

}
