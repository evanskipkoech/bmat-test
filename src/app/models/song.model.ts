export class Song {
    constructor(
        public artist?: string,
        public title?: string,
        public isrc?: number,
        public duration?: string
    ) { }
}