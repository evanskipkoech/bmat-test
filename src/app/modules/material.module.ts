import { NgModule } from '@angular/core';
import {
    MatTabsModule,
    MatTableModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatIconModule,
    MatTooltipModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule
} from '@angular/material';

@NgModule({
    imports: [
        MatTabsModule,
        MatTableModule,
        MatCheckboxModule,
        MatIconModule,
        MatSnackBarModule,
        MatTooltipModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule],
    exports: [
        MatTabsModule,
        MatTableModule,
        MatCheckboxModule,
        MatIconModule,
        MatSnackBarModule,
        MatTooltipModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule
    ],

})

export class MaterialModule { }