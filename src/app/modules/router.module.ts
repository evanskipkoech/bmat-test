import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SongListComponent } from '../components/song-list/song-list.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'song-list', pathMatch: 'full' },
    { path: 'song-list', component: SongListComponent },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(
            appRoutes, { onSameUrlNavigation: 'reload' }
        ),
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class RoutingModule { }