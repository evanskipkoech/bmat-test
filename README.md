# BMAT Frontend Test
This is a sample project built for the skills assesment test for the Frontend Engineer position at BMAT.
It is built on [Angular CLI](https://github.com/angular/angular-cli) version 7.0.1. with [Angular material UI](https://v6.material.angular.io/) for the look and feel and [Bootstrap 5](https://getbootstrap.com/docs/5.0/getting-started/introduction/) for responsivess and structuring UI components.

### Steps to run the application
1. Clone repo to your local environment.
2. Ensure you have Angular v7 installed. You can check out the [Angular documentation](https://v7.angular.io/guide/quickstart) one how to get started.
3. From the project root directory run `npm install` to install the app dependency’s.
4. Run `ng serve —o` for a dev server. This should automatically open the app on `http://localhost:4200/` on your browser.

### How to test the application
1. Upload the candidate csv file, `sound_recording_input_report.csv`, on the file input provided.
2. Click on the `Upload catalogue` button to process the file.
3. The table should update in real time adding the missing metadata to the existing records.

## Possibile improvements
### Technical improvements
1. Configure a CI/CD pipeline.
2. Dockerize the application for easy deployment.
3. Refactoring methods to extrapolate repeated functionality and separate concerns.
4. Persistance on the data to read from new data for a live update on the UI table

### UI improvements
1. Show visual cues for rows affected after uploading the upload to aid with change blindness. (e.g. change row colours for a few seconds after the update is complete)
2. Add a progress indicator to show the upload progress.
3. Allow for paginating the data with the increase of data.
4. Improve the visual design to adopt to a particular brand inline with brand guide lines to improve the look and feel.

### Assumptions
1. Input always has the `artist name` and `song title` which forms the basis of `key` in the hashmap search function.
